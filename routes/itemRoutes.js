const _ = require('lodash');

let {Item} = require('../models/item');

module.exports = (app) => {
  app.get('/items', (req, res) => {
    Item.find({}).then((items) => {
      res.send({items});
    }, (error) => {
      res.status(400).send();
    });
  });

  app.post('/items', (req, res) => {
    let items = _.pick(req.body, ['title', 'description']);
    console.log(items);
    let item = new Item(items);
    item.save().then((item) => {
      res.send({item});
    }, (error) => {
      res.status(400).send();
    });
  });

  app.get('/items/:id', (req, res) => {
    let id = req.params.id;
    Item.findById(id).then((item) => {
      res.send({item});
    }).catch((error) => {
      res.status(400).send();
    });
  });

  app.patch('/items/:id', (req, res) => {
    let id = req.params.id;
    let body = _.pick(req.body, ['title', 'description']);

    Item.updateOne({_id: id}, {$set: body}, {new: true}).then((item) => {
      res.send({item});
    }).catch((error) => {
      res.status(400).send();
    });
  });

  app.delete('/items/:id', (req, res) => {
    let id = req.params.id;
    Item.deleteOne({_id: id}).then((item) => {
      res.send({deleted: true});
    }).catch((error) => {
      res.status(400).send();
    });
  });
};
