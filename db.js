let mongoose = require('mongoose');

mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost/BibleAPI', {useNewUrlParser: true});

module.exports = mongoose;
