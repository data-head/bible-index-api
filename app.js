let express = require('express');
let mongoose = require('./db.js');

let app = express();
let bodyParser = require('body-parser');

let Item = require('./routes/itemRoutes');

app.use(bodyParser.json());

app.use(express.static('public'));

app.get('/', (req, res) => {
  res.send({message: 'Hello World! This is the bible index API'});
});

Item(app);

app.listen(3000, () => {
  console.log('Listening on port 3000...');
});
