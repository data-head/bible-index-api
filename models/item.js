let mongoose = require('mongoose');

let ItemSchema = new mongoose.Schema({
  title: {
    type: String,
    required: true
  },
  description: {
    type: String,
    required: true
  }
});

let Item = mongoose.model('Item', ItemSchema);

module.exports = {
  Item
};
